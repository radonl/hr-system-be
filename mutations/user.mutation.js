const graphql = require('graphql');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {GraphQLString, GraphQLNonNull, GraphQLBoolean} = graphql;
const moment = require('moment');
const ObjectId = require('mongoose').Types.ObjectId;

const UserModel = require('../models/user.model');
const UserType = require('../types/user.type');
const FileInput = require('../inputs/file.input');
const storage = require('../services/storage');

module.exports = {
  createUser: {
    type: UserType,
    args: {
      firstName: {
        type: GraphQLNonNull(GraphQLString)
      },
      lastName: {
        type: GraphQLNonNull(GraphQLString)
      },
      middleName: {
        type: GraphQLString
      },
      avatarFilePath: {
        type: GraphQLString
      },
      roleId: {
        type: GraphQLNonNull(GraphQLString)
      },
      email: {
        type: GraphQLNonNull(GraphQLString)
      },
      avatar: {
        type: FileInput
      },
      password: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: async (parent, args) => {
      try {
        const user = new UserModel({
          ...args,
          ...args.avatar ? {
            avatarFilePath: await storage.createFile(args.avatar.fileExtension, args.avatar.fileContent, 'users')
          }: {}
        });
        await user.save();
        return user;
      } catch (e) {
        console.log(e);
      }
    }
  },
  updateUser: {
    type: UserType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      },
      firstName: {
        type: GraphQLNonNull(GraphQLString)
      },
      lastName: {
        type: GraphQLNonNull(GraphQLString)
      },
      middleName: {
        type: GraphQLString
      },
      avatar: {
        type: FileInput
      },
      roleId: {
        type: GraphQLNonNull(GraphQLString)
      },
      email: {
        type: GraphQLNonNull(GraphQLString)
      },
      password: {
        type: GraphQLString
      }
    },
    resolve: async (parent, {_id,firstName,lastName,middleName,avatar,roleId,email,password}) => {
      try {
        const user = await UserModel.findOne({_id});
        user.firstName = firstName;
        user.lastName = lastName;
        user.middleName = middleName;
        user.roleId = roleId;
        user.email = email;
        if(password) {
          user.password = password;
        }
        if(avatar) {
          user.avatarFilePath = await storage.createFile(avatar.fileExtension, avatar.fileContent, 'users');
        }
        await user.save();
        return user;
      } catch (e) {
        console.log(e);
      }
    }
  },
  removeUser: {
    type: UserType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: async (parent, {_id}) => {
      return await UserModel.findOne({
        _id: ObjectId(_id)
      }).remove();
    }
  },
  login: {
    type: GraphQLString,
    args: {
      email: {
        type: GraphQLNonNull(GraphQLString)
      },
      password: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: async (parent, {email, password}) => {
      try {
        const user = await UserModel.findOne({email});
        if (!user) {
          return false;
        }
        const isPasswordMatch = await bcrypt.compare(password, user.password);
        if (!isPasswordMatch) {
          return false;
        }
        const token = jwt.sign({
          firstName: user.firstName,
          lastName: user.lastName,
          roleId: user.roleId,
          email: user.email,
          _id: user._id
        }, process.env.JWT, {
          expiresIn: '1d'
        });
        user.tokens = user.tokens.concat({
          token,
          expireOn: moment().add(1, 'days'),
          inactive: false
        });
        user.save();
        return token;
      } catch (e) {
        console.log(e);
      }
    }
  },
  logout: {
    type: GraphQLBoolean,
    resolve: async (parent, args, {user, token}) => {
      try {
        const userDb = await UserModel.findOne({
          _id: ObjectId(user._id)
        });
        userDb.tokens = userDb.tokens.map(item => {
          if(item.token === token) {
            item.inactive = true;
          }
          return item;
        });
        await userDb.save();
        return true;
      } catch (e) {
        return false;
      }
    }
  }
};
