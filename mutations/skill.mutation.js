const graphql = require('graphql');
const {GraphQLString, GraphQLNonNull} = graphql;

const SkillModel = require('../models/skill.model');
const SkillType = require('../types/skill.type');

module.exports = {
  createSkill: {
    type: SkillType,
    args: {
      name: {
        type: GraphQLNonNull(GraphQLString)
      },
    },
    resolve: (parent, args) => {
      const skill = new SkillModel(args);
      skill.save();
      return skill;
    }
  },
  updateSkill: {
    type: SkillType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      },
      name: {
        type: GraphQLString
      },
    },
    resolve: (parent, {id, name}) => {
      const skill = SkillModel.findOne({_id: ObjectId(_id)});
      skill.name = name;
      skill.save();
      return skill;
    }
  },
  removeSkill: {
    type: SkillType,
    args: {
      id: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {id}) => {
      return SkillModel.findOne({
        _id: ObjectId(id)
      }).remove();
    }
  }
};
