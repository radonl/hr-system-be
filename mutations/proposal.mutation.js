const graphql = require('graphql');
const {GraphQLString, GraphQLNonNull, GraphQLFloat, GraphQLList} = graphql;
const ObjectId = require('mongoose').Types.ObjectId;
const ProposalModel = require('../models/proposal.model');
const ProposalType = require('../types/proposal.type');
const FileInput = require('../inputs/file.input');
const storage = require('../services/storage');

module.exports = {
  updateProposal: {
    type: ProposalType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      },
      firstName: {
        type: GraphQLNonNull(GraphQLString)
      },
      lastName: {
        type: GraphQLNonNull(GraphQLString)
      },
      middleName: {
        type: GraphQLString
      },
      email: {
        type: GraphQLNonNull(GraphQLString)
      },
      avatar: {
        type: FileInput
      },
      cv: {
        type: FileInput
      },
      skills: {
        type: GraphQLList(GraphQLString)
      },
      techEmployees: {
        type: GraphQLList(GraphQLString)
      },
      desiredSalary: {
        type: GraphQLFloat
      },
      suggestedSalary: {
        type: GraphQLFloat
      },
      scheduledInterviewTime: {
        type: GraphQLString
      },
      status: {
        type: GraphQLString
      },
      phone: {
        type: GraphQLString
      }
    },
    resolve: async (parent, args) => {
      try {
        const [avatarFilePath, cvFilePath] = await Promise.all([
          storage.createFile(args.avatar.fileExtension, args.avatar.fileContent, 'users'),
          storage.createFile(args.cv.fileExtension, args.cv.fileContent, 'proposals')
        ]);
        const proposal = await ProposalModel.findOne({
          _id: ObjectId(args._id)
        });
        proposal.techEmployees = args.techEmployees;
        proposal.skills = args.skills;
        proposal.status = args.status;
        proposal.desiredSalary = args.desiredSalary;
        proposal.suggestedSalary = args.suggestedSalary;
        proposal.scheduledInterviewTime = args.scheduledInterviewTime;
        proposal.cvFilePath = cvFilePath;
        proposal.candidate = {
          firstName: args.firstName,
          lastName: args.lastName,
          middleName: args.middleName,
          email: args.email,
          phone: args.phone,
          avatarFilePath
        };
        await proposal.save();
        return proposal;
      } catch (e) {
        console.error(e);
      }
    }
  },
  removeProposal: {
    type: ProposalType,
    args: {
      id: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {id}) => {
      try {
        return ProposalModel.findOne({
          _id: ObjectId(id)
        }).remove();
      } catch (e) {
        console.log(e);
      }
    }
  },
  createProposal: {
    type: ProposalType,
    args: {
      firstName: {
        type: GraphQLNonNull(GraphQLString)
      },
      lastName: {
        type: GraphQLNonNull(GraphQLString)
      },
      middleName: {
        type: GraphQLString
      },
      email: {
        type: GraphQLNonNull(GraphQLString)
      },
      phone: {
        type: GraphQLString
      },
      avatar: {
        type: FileInput
      },
      cv: {
        type: FileInput
      },
      skills: {
        type: GraphQLList(GraphQLString)
      },
      techEmployees: {
        type: GraphQLList(GraphQLString)
      },
      desiredSalary: {
        type: GraphQLFloat
      },
      suggestedSalary: {
        type: GraphQLFloat
      },
      scheduledInterviewTime: {
        type: GraphQLString
      }
    },
    resolve: async (parent, args, context) => {
      try {
        const [avatarFilePath, cvFilePath] = await Promise.all([
          storage.createFile(args.avatar.fileExtension, args.avatar.fileContent, 'users'),
          storage.createFile(args.cv.fileExtension, args.cv.fileContent, 'proposals')
        ]);
        const proposal = new ProposalModel({
          techEmployees: args.techEmployees,
          skills: args.skills,
          status: 'Waiting for an interview',
          initiator: context.user._id,
          desiredSalary: args.desiredSalary,
          suggestedSalary: args.suggestedSalary,
          scheduledInterviewTime: args.scheduledInterviewTime,
          reviews: [],
          cvFilePath,
          candidate: {
            firstName: args.firstName,
            lastName: args.lastName,
            middleName: args.middleName,
            email: args.email,
            phone: args.phone,
            avatarFilePath
          }
        });
        await proposal.save();
        return proposal;
      } catch (e) {
        console.error(e);
      }
    }
  },
  createReview: {
    type: ProposalType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      },
      message: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: async (parent, {_id, message}, {user}) => {
      const proposal = await ProposalModel.findOne({_id});
      proposal.reviews.push({reviewer: user._id, message});
      return await proposal.save();
    }
  }
};
