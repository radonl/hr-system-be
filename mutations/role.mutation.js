const graphql = require('graphql');
const {GraphQLString, GraphQLNonNull} = graphql;
const ObjectId = require('mongoose').Types.ObjectId;

const RoleModel = require('../models/role.model');
const RoleType = require('../types/role.type');

module.exports = {
  createRole: {
    type: RoleType,
    args: {
      name: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {name}) => {
      const role = new RoleModel({name});
      role.save();
      return role;
    }
  },
  updateRole: {
    type: RoleType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      },
      name: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: async (parent, {_id, name}) => {
      const role = await RoleModel.findOne({_id: ObjectId(_id)});
      role.name = name;
      role.save();
      return role;
    }
  },
  removeRole: {
    type: RoleType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {_id}) => {
      return RoleModel.findOne({
        _id: ObjectId(id)
      }).remove();
    }
  }
};
