const graphql = require('graphql');
const {GraphQLObjectType, GraphQLString} = graphql;

const storage = require('../services/storage');

const CandidateType = new GraphQLObjectType({
  name: `Candidate`,
  description: `Candidate`,
  fields: () => ({
    firstName: {
      type: GraphQLString
    },
    lastName: {
      type: GraphQLString
    },
    middleName: {
      type: GraphQLString
    },
    email: {
      type: GraphQLString
    },
    phone: {
      type: GraphQLString
    },
    avatarFilePath: {
      type: GraphQLString
    }
  })
});

module.exports = CandidateType;
