const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLObjectType, GraphQLString} = graphql;

const Role = require('../models/role.model');
const storage = require('../services/storage');

const UserType = new GraphQLObjectType({
  name: `User`,
  description: `User`,
  fields: () => ({
    _id: {
      type: GraphQLString
    },
    firstName: {
      type: GraphQLString
    },
    lastName: {
      type: GraphQLString
    },
    middleName: {
      type: GraphQLString
    },
    email: {
      type: GraphQLString
    },
    name: {
      type: GraphQLString,
      resolve: ({firstName, lastName}) => {
        return `${firstName} ${lastName}`;
      }
    },
    avatarFilePath: {
      type: GraphQLString
    },
    role: {
      type: RoleType(),
      resolve: ({roleId}, args) => {
        return Role.findOne({
          _id: ObjectId(roleId)
        });
      }
    },
  })
});

const RoleType = () => require('./role.type');

module.exports = UserType;
