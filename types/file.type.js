const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLObjectType, GraphQLString} = graphql;

const FileType = new GraphQLObjectType({
  name: `File`,
  description: `File`,
  fields: () => ({
    fileName: {
      type: GraphQLString,
    },
    fileContent: {
      type: GraphQLString,
    }
  })
});


module.exports = FileType;
