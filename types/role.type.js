const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLObjectType, GraphQLString, GraphQLList} = graphql;

const User = require('../models/user.model');

const RoleType = new GraphQLObjectType({
  name: `Role`,
  description: `Role`,
  fields: () => ({
    _id: {
      type: GraphQLString
    },
    name: {
      type: GraphQLString
    },
    users: {
      type: GraphQLList(UserType()),
      resolve: ({_id}, args) => {
        return User.find({
          roleId: ObjectId(_id)
        });
      }
    }
  })
});

const UserType = () => require('./user.type');

module.exports = RoleType;
