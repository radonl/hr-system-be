const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLObjectType, GraphQLString, GraphQLList} = graphql;

const User = require('../models/user.model');

const ReviewType = new GraphQLObjectType({
  name: `Review`,
  description: `Review`,
  fields: () => ({
    _id: {
      type: GraphQLString
    },
    message: {
      type: GraphQLString
    },
    reviewer: {
      type: UserType(),
      resolve: ({reviewer}, args) => {
        return User.findOne({
          _id: ObjectId(reviewer)
        });
      }
    }
  })
});

const UserType = () => require('./user.type');

module.exports = ReviewType;
