const moment = require('moment');
const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLObjectType, GraphQLString, GraphQLFloat, GraphQLList} = graphql;

const CandidateType = require('./candidate.type');
const SkillType = require('./skill.type');
const ReviewType = require('./review.type');
const FileType = require('./file.type');
const storage = require('../services/storage');

const ProposalType = new GraphQLObjectType({
  name: `Proposal`,
  description: `Proposal`,
  fields: () => ({
    _id: {
      type: GraphQLString
    },
    desiredSalary: {
      type: GraphQLFloat
    },
    suggestedSalary: {
      type: GraphQLFloat
    },
    scheduledInterviewTime: {
      type: GraphQLString,
      resolve: ({scheduledInterviewTime}) => {
        return moment(scheduledInterviewTime).format('YYYY-MM-DD HH:mm')
      }
    },
    status: {
      type: GraphQLString
    },
    candidate: {
      type: CandidateType,
    },
    techEmployees: {
      type: GraphQLList(UserType())
    },
    initiator: {
      type: UserType(),
    },
    skills: {
      type: GraphQLList(SkillType)
    },
    cv: {
      type: FileType,
      resolve: async ({cvFilePath}) => {
        if(cvFilePath) {
          return {
            fileName: cvFilePath.split('/')[2],
            fileContent: await storage.getFile(cvFilePath)
          }
        }
      }
    },
    reviews: {
      type: GraphQLList(ReviewType)
    }
  })
});

const UserType = () => require('./user.type');

module.exports = ProposalType;
