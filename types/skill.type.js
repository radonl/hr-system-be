const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLObjectType, GraphQLString} = graphql;

const SkillType = new GraphQLObjectType({
  name: `Skill`,
  description: `Skill`,
  fields: () => ({
    _id: {
      type: GraphQLString
    },
    name: {
      type: GraphQLString
    }
  })
});

module.exports = SkillType;
