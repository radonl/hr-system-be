const graphql = require('graphql');
const {applyMiddleware} = require('graphql-middleware');
const {GraphQLObjectType, GraphQLSchema, GraphQLString} = graphql;

const ProposalQuery = require('./queries/proposal.query');
const RoleQuery = require('./queries/role.query');
const SkillQuery = require('./queries/skill.query');
const UserQuery = require('./queries/user.query');
const RoleMutation = require('./mutations/role.mutation');
const UserMutation = require('./mutations/user.mutation');
const SkillMutation = require('./mutations/skill.mutation');
const ProposalMutation = require('./mutations/proposal.mutation');
const {permissions} = require('./permissions/permissions');

const Query = new GraphQLObjectType({
  name: `Query`,
  fields: {
    ...ProposalQuery,
    ...RoleQuery,
    ...SkillQuery,
    ...UserQuery
  }
});


const Mutation = new GraphQLObjectType({
  name: `Mutation`,
  fields: {
    ...RoleMutation,
    ...UserMutation,
    ...SkillMutation,
    ...ProposalMutation,
  }
});


module.exports = applyMiddleware(new GraphQLSchema({
  query: Query,
  mutation: Mutation,
}), permissions);
