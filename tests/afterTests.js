const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_TESTS, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const ProposalModel = require('../models/proposal.model');
const UserModel = require('../models/user.model');
const RoleModel = require('../models/role.model');
const SkillModel = require('../models/skill.model');

module.exports = async () => {
  await Promise.all([
    ProposalModel.remove({}),
    UserModel.remove({}),
    RoleModel.remove({}),
    SkillModel.remove({})
  ]);
  await db.disconnect();
};