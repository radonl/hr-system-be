// const gql = require('graphql-tag');
//
// const beforeTests = require('./beforeTests');
// const afterTests = require('./afterTests');
// const SkillModel = require('../models/skill.model');
//
// jest.setTimeout(30000);
//
// const GET_SKILLS = gql`
//   {
//     skills {
//        name
//     }
//   }
// `;
//
// const GET_SKILL = gql`query ($_id:String!) {
//   skill(_id:$_id) {
//     name
//   }
// }`;
//
// const CREATE_SKILL = gql`mutation ($name:String!) {
//   createSkill(name: $name) {
//     name
//   }
// }`;
//
// const UPDATE_SKILL = gql`mutation($_id: String!, $name: String) {
//   updateSkill(_id: $_id, name: $name) {
//     name
//   }
// }
// `;
//
// const REMOVE_SKILL = gql`mutation($_id: String!) {
//   removeSkill(_id: $_id) {
//     name
//     _id
//   }
// }
// `;
//
//
// let client, roles;
//
// describe('skills test', () => {
//   beforeAll(async () => {
//     const main = await beforeTests();
//     client = main.client;
//     roles = main.roles;
//   });
//   afterAll(async () => {
//     await afterTests();
//   });
//   fit('get skills match snapshot', async () => {
//     const skills = await client.query({query: GET_SKILLS});
//     expect(skills).toMatchSnapshot();
//   });
//   fit('get skill match snapshot', async () => {
//     const skillDb = await SkillModel.findOne({name: 'JavaScript'});
//     const skill = await client.query({query: GET_SKILL, variables: {_id: skillDb._id.toString()}});
//     expect(skill).toMatchSnapshot();
//   });
//   it('create skill', async () => {
//     const res = await client.mutate({mutation: CREATE_SKILL, variables: {name: 'test'}});
//     expect(res.data.createSkill).toEqual({name: 'test'});
//   });
//   it('update skill', async () => {
//     const skillDb = await SkillModel.findOne({name: 'JavaScript'});
//     const res = await client.mutate({mutation: UPDATE_SKILL, variables: {name: 'test', _id: skillDb._id.toString()}});
//     expect(res.data.updateSkill).toEqual({name: 'test'});
//   });
//   it('remove skill', async () => {
//     const skillDb = await SkillModel.findOne({});
//     const res = await client.mutate({mutation: REMOVE_SKILL, variables: {id: skillDb._id.toString()}});
//     expect(res.data.removeSkill).toEqual(skillDb);
//   });
// });