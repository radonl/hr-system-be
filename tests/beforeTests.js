require('dotenv').config();
const mongoose = require('mongoose');
const {createTestClient} = require('apollo-server-testing');
const {ApolloServer} = require('apollo-server');
const moment = require('moment');
const jwt = require('jsonwebtoken');

const schema = require('../schema');
const {getClaims} = require('../permissions/permissions');
const Role = require('../models/role.model');
const UserModel = require('../models/user.model');

mongoose.connect(process.env.MONGO_TESTS, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const getUserAndTokenByRole = async (role) => {
  try {
    const user = await UserModel.findOne({roleId: role.id});
    const token = jwt.sign({
      firstName: user.firstName,
      lastName: user.lastName,
      roleId: user.roleId,
      email: user.email,
      _id: user._id
    }, process.env.JWT, {
      expiresIn: '1d'
    });
    return {user, token};
  } catch (e) {
    console.error(e);
  }
};

module.exports = async () => {
  await require('../seeds')();
  return new Promise((resolve, reject) => {
    setTimeout(async () => {
      const [roleAdministrator, roleHR, roleTechEmployee] = await
      Promise.all([
        Role.findOne({name: "Administrator"}),
        Role.findOne({name: "HR"}),
        Role.findOne({name: "Tech Employee"})
      ]);
      console.log(roleAdministrator, roleHR);

      const {user, token} = await getUserAndTokenByRole(roleAdministrator);
      const roles = {roleAdministrator, roleHR, roleTechEmployee};

      const client = createTestClient(new ApolloServer({
        schema,
        debug: true,
        context: req => {
          return {
            req: {
              header: (prop) => {
                return token;
              }
            },
            user,
            roles
          }
        }
      }));
      resolve({client, roles});
    }, 500)
  })
};