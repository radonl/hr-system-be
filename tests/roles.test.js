// const gql = require('graphql-tag');
//
// const beforeTests = require('./beforeTests');
// const afterTests = require('./afterTests');
// const RoleModel = require('../models/role.model');
//
// let client, roles;
//
// const GET_ROLES = gql`{
//       roles {
//         name
//       }
//     }`;
//
// const GET_ROLE = gql`query ($_id:String!) {
//   role (_id:$_id) {
//     name
//   }
// }`;
//
// const CREATE_ROLE = gql`
//       mutation($name:String!) {
//         createRole(name:$name) {
//           name
//         }
//       }
//     `;
//
// const UPDATE_ROLE = gql`
//       mutation($name:String!, $_id:String!) {
//         updateRole(name:$name, _id:$_id) {
//           _id
//           name
//         }
//       }
//     `;
//
// const REMOVE_ROLE = gql`mutation ($_id:String!) {
//   removeRole(id: $_id) {
//     name
//   }
// }`;
//
// describe('roles test', () => {
//   beforeAll(async () => {
//     const main = await beforeTests();
//     client = main.client;
//     roles = main.roles;
//   });
//   afterAll(async () => {
//     await afterTests();
//   });
//
//   fit('get roles match snapshot', async () => {
//     const roles = await client.query({ query: GET_ROLES });
//     expect(roles).toMatchSnapshot();
//   });
//
//   fit('get role match snapshot', async () => {
//     const role = await client.query({query: GET_ROLE, variables: {_id: roles.roleTechEmployee.id}});
//     expect(role).toMatchSnapshot();
//   });
//
//   it('create role', async () => {
//     const res = await client.mutate({mutation: CREATE_ROLE, variables: {name: 'test'}});
//     expect(res.data.createRole).toEqual({name: 'test'});
//   });
//
//   it('update role', async () => {
//     const res = await client.mutate({mutation: UPDATE_ROLE, variables: {
//       name: 'test',
//       _id: roles.roleTechEmployee.id
//     }});
//     expect(res.data.updateRole).toEqual({
//       name: 'test',
//       _id: roles.roleTechEmployee.id
//     });
//   });
//
//   it('remove role', async () => {
//     const role = await RoleModel.findOne({name: 'HR'});
//     const res = await client.mutate({mutation: REMOVE_ROLE, variables: {_id: role.id}});
//     expect(res.data.removeRole).toEqual({
//       name: 'HR'
//     });
//   })
// });