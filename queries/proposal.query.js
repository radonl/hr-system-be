const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLString, GraphQLList, GraphQLNonNull, GraphQLInt} = graphql;

const Proposal = require('../models/proposal.model');
const ProposalType = require('../types/proposal.type');

module.exports = {
  proposal: {
    type: ProposalType,
    args: {
      _id: {
        type: GraphQLString
      }
    },
    resolve: (parent, {_id}) => {
      return Proposal.findOne({
        _id: ObjectId(_id)
      });
    }
  },
  proposals: {
    type: GraphQLList(ProposalType),
    args: {
      skip: {
        type: GraphQLNonNull(GraphQLInt)
      },
      limit: {
        type: GraphQLNonNull(GraphQLInt)
      },
      searchString: {
        type: GraphQLString
      },
      order: {
        type: GraphQLString
      },
      orderBy: {
        type: GraphQLString
      }
    },
    resolve: async (parent, {skip, limit, searchString, order, orderBy}, {user, roles}) => {
      try {
        const searchQuery = {
          $regex: searchString ? searchString : '', $options: 'i'
        };
        const search = {
          $or: [{
            'candidate.firstName': searchQuery
          },{
            'candidate.middleName': searchQuery
          },{
            'candidate.lastName': searchQuery
          }]
        };
        return await Proposal.find({
          ...(user.roleId === roles.roleTechEmployee.id) ? {techEmployees: user._id} : {},
          ...searchString ? search : {}
        }).populate(['skills', 'techEmployees', 'initiator'])
          .sort([[orderBy, order ? (order === 'asc' ? 1 : -1) : 0]])
          .skip(skip).limit(limit);
      } catch (e) {
        console.log(e);
      }
    }
  },
  proposalsCount: {
    type: GraphQLNonNull(GraphQLInt),
    args: {
      searchString: {
        type: GraphQLString
      }
    },
    resolve: async (parent, {searchString}, {user, roles}) => {
      try {
        const searchQuery = {
          $regex: searchString ? searchString : '', $options: 'i'
        };
        const search = {
          $or: [{
            'candidate.firstName': searchQuery
          },{
            'candidate.middleName': searchQuery
          },{
            'candidate.lastName': searchQuery
          }]
        };
        return await Proposal.count({
          ...(user.roleId === roles.roleTechEmployee.id) ? {techEmployees: user._id} : {},
          ...searchString ? search : {}
        }).populate(['skills', 'techEmployees', 'initiator']);
      } catch (e) {
        console.log(e);
      }
    }
  },
};
