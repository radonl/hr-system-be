const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLString, GraphQLList} = graphql;

const Skill = require('../models/skill.model');
const SkillType = require('../types/skill.type');

module.exports = {
  skill: {
    type: SkillType,
    args: {
      _id: {
        type: GraphQLString
      }
    },
    resolve: (parent, {_id}) => {
      return Skill.findOne({
        _id: ObjectId(_id)
      });
    }
  },
  skills: {
    type: GraphQLList(SkillType),
    resolve: async () => {
     try{
       return await Skill.find({});
     }catch (e) {
       console.log(e);
     }
    }
  },
};
