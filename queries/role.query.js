const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLString, GraphQLList} = graphql;

const Role = require('../models/role.model');
const RoleType = require('../types/role.type');

module.exports = {
  role: {
    type: RoleType,
    args: {
      _id: {
        type: GraphQLString
      }
    },
    resolve: (parent, {_id}) => {
      return Role.findOne({
        _id: ObjectId(_id)
      });
    }
  },
  roles: {
    type: GraphQLList(RoleType),
    resolve: () => {
      return Role.find({});
    }
  },
};
