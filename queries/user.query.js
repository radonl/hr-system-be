const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLString, GraphQLList, GraphQLBoolean, GraphQLInt, GraphQLNonNull} = graphql;

const User = require('../models/user.model');
const UserType = require('../types/user.type');

module.exports = {
  user: {
    type: UserType,
    args: {
      _id: {
        type: GraphQLString
      }
    },
    resolve: (parent, {_id}) => {
      return User.findOne({
        _id: ObjectId(_id)
      });
    }
  },
  users: {
    type: GraphQLList(UserType),
    args: {
      onlyHRs: {
        type: GraphQLBoolean
      },
      onlyTechEmployees: {
        type: GraphQLBoolean
      },
      skip: {
        type: GraphQLNonNull(GraphQLInt)
      },
      limit: {
        type: GraphQLNonNull(GraphQLInt)
      },
      order: {
        type: GraphQLString
      },
      orderBy: {
        type: GraphQLString
      },
      searchString: {
        type: GraphQLString
      }
    },
    resolve:async (parent, {onlyHRs,onlyTechEmployees, skip, limit, order, orderBy, searchString}, {roles}) => {
      try {
        return await User.find({
          ...onlyHRs ? {roleId: roles.roleHR._id} : {},
          ...onlyTechEmployees ? {roleId: roles.roleTechEmployee._id} : {},
          ...searchString ? {$or: [
              {
                firstName: {
                  $regex: searchString, $options: 'i'
                }
              },
              {
                lastName: {
                  $regex: searchString, $options: 'i'
                }
              },
              {
                middleName: {
                  $regex: searchString, $options: 'i'
                }
              }
            ]} : {}
        })
          .sort([[orderBy, order ? (order === 'asc' ? 1 : -1) : 0]])
          .skip(skip)
          .limit(limit);
      } catch (e) {
        console.log(e);
      }
    }
  },
  usersCount: {
    type: GraphQLNonNull(GraphQLInt),
    args: {
      onlyHRs: {
        type: GraphQLBoolean
      },
      onlyTechEmployees: {
        type: GraphQLBoolean
      },
      searchString: {
        type: GraphQLString
      }
    },
    resolve: (parent, {onlyHRs, onlyTechEmployees, searchString}, {roles}) => {
      return User.count({
        ...onlyHRs ? {roleId: roles.roleHR._id} : {},
        ...onlyTechEmployees ? {roleId: roles.roleTechEmployee._id} : {},
        ...searchString ? {$or: [
            {
              firstName: {
                $regex: searchString, $options: 'i'
              }
            },
            {
              lastName: {
                $regex: searchString, $options: 'i'
              }
            },
            {
              middleName: {
                $regex: searchString, $options: 'i'
              }
            }
          ]} : {}
      })
    }
  },
  me: {
    type: UserType,
    resolve: async (parent, args, context) => {
      return await User.findOne({
        _id: ObjectId(context.user._id)
      });
    }
  }
};
