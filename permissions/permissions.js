const jwt = require('jsonwebtoken');
const { rule, and, or, shield} = require("graphql-shield");

const Role = require('../models/role.model');
const UserModel = require('../models/user.model');

const getClaims = async (req) => {
  try {
    const token = req.req.header("JWT");
    let user = null;
    if(token) {
      try {
        user = jwt.verify(token, process.env.JWT);
        const userDb = await UserModel.findOne({
          _id: user._id
        });
        if(!userDb || !userDb.tokens) {
          throw new Error('unauthorized');
        }
        const currentTokenInDb = userDb.tokens.find(item => item.token === token);
        if(!currentTokenInDb || currentTokenInDb.inactive) {
          throw new Error('unauthorized');
        }
      } catch (e) {
        req.res.status(401);
        return req;
      }
    }
    const [roleAdministrator, roleHR, roleTechEmployee] = await Promise.all([
      Role.findOne({name:"Administrator"}),
      Role.findOne({name:"HR"}),
      Role.findOne({name:"Tech Employee"})
    ]);
    return {
      ...req,
      user,
      token,
      roles: {roleAdministrator, roleHR, roleTechEmployee}
    }
  } catch (e) {
    return req;
  }
};

const isAuthenticated = rule()(async (parent, args, ctx, info) => {
  return !!ctx.user;
});

const isAdministrator = rule()(async (parent, args, ctx, info) => {
  return ctx.user.roleId === ctx.roles.roleAdministrator.id;
});

const isHR = rule()(async (parent, args, ctx, info) => {
  return ctx.user.roleId === ctx.roles.roleHR.id;
});

const isTechEmployee = rule()(async (parent, args, ctx, info) => {
  return ctx.user.roleId === ctx.roles.roleTechEmployee.id;
});

const permissions = shield({
  Query: {
    proposal: and(isAuthenticated, or(isAdministrator, isHR, isTechEmployee)),
    proposals: and(isAuthenticated, or(isAdministrator, isHR, isTechEmployee)),
    role: and(isAuthenticated, or(isAdministrator, isHR, isTechEmployee)),
    skill: and(isAuthenticated, or(isAdministrator, isHR, isTechEmployee)),
    skills: and(isAuthenticated, or(isAdministrator, isHR, isTechEmployee)),
    user: and(isAuthenticated, or(isAdministrator, isHR, isTechEmployee)),
    users: and(isAuthenticated, or(isAdministrator, isHR, isTechEmployee))
  },
  Mutation: {
    createRole: and(isAuthenticated, isAdministrator),
    updateRole: and(isAuthenticated, isAdministrator),
    removeRole: and(isAuthenticated, isAdministrator),
    createUser: and(isAuthenticated, isAdministrator),
    updateUser: and(isAuthenticated, isAdministrator),
    removeUser: and(isAuthenticated, isAdministrator),
    createSkill: and(isAuthenticated, or(isAdministrator, isHR)),
    updateSkill: and(isAuthenticated, or(isAdministrator, isHR)),
    removeSkill: and(isAuthenticated, or(isAdministrator, isHR)),
    updateProposal: and(isAuthenticated, or(isAdministrator, isHR)),
    removeProposal: and(isAuthenticated, or(isAdministrator, isHR)),
    createProposal: and(isAuthenticated, or(isAdministrator, isHR)),
    createReview: and(isAuthenticated, or(isAdministrator, isHR, isTechEmployee)),
  }
});

module.exports = {getClaims, permissions};

