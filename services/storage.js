const crypto = require('crypto');
const axios = require('axios');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});


class Storage {
  createFile(fileExtension, base64, dir) {
    return new Promise(((resolve, reject) => {
      if (fileExtension && base64 && dir) {
        const fileName = `${crypto.createHash('md5').update(base64).digest('hex')}.${fileExtension}`;
        const path = `storage/${dir}`;
        const buffer = Buffer.from(base64, 'base64');
        s3.upload({
          Bucket: process.env.AWS_BUCKET,
          Key: `${path}/${fileName}`,
          Body: buffer
        }, (err, data) => {
          if (err) {
            reject(err);
          }
          resolve(data.Location)
        });
      } else {
        resolve('');
      }
    }));
  }

  async getFile(path) {
    if (path) {
      try {
        return new Buffer((await axios.get(path, {
          responseType: 'arraybuffer'
        })).data, 'binary').toString('base64')
      } catch (e) {
        return false;
      }
    }
    return false;
  }
}

module.exports = new Storage();