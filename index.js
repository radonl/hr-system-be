require('dotenv').config();
const {ApolloServer} = require('apollo-server');
const schema = require('./schema');

const {getClaims} = require('./permissions/permissions');

const server = new ApolloServer({
  schema,
  context: getClaims,
  playground: true,
});

const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

if(process.env.RUN_SEEDS === 'true') {
  console.log('Seeds...');
  require('./seeds')()
}

server.listen(process.env.PORT).then(({url}) => {
  console.log(`🚀  Server ready at ${url}`);
});
