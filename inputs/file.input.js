const graphql = require('graphql');
const {GraphQLInputObjectType, GraphQLString} = graphql;

const FileInput = new GraphQLInputObjectType({
  name: `FileInput`,
  description: `FileInput`,
  fields: () => ({
    fileExtension: {
      type: GraphQLString
    },
    fileContent: {
      type: GraphQLString
    }
  })});

module.exports = FileInput;
