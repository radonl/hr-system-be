const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RoleModelSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
});

module.exports = mongoose.model('Role', RoleModelSchema);
