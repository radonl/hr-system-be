const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SkillModelSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
});

module.exports = mongoose.model('Skill', SkillModelSchema);
