const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProposalModelSchema = new Schema({
  cvFilePath: {
    type: String,
    trim: true
  },
  status: {
    type: String,
    required: true,
    trim: true
  },
  desiredSalary: {
    type: Schema.Types.Number,
    required: true
  },
  suggestedSalary: {
    type: Schema.Types.Number
  },
  scheduledInterviewTime: {
    type: Schema.Types.Date,
  },
  candidate: {
    firstName: {
      type: Schema.Types.String,
      required: true
    },
    lastName: {
      type: Schema.Types.String,
      required: true
    },
    middleName: {
      type: Schema.Types.String
    },
    email: {
      type: Schema.Types.String
    },
    phone: {
      type: Schema.Types.String
    },
    avatarFilePath: {
      type: Schema.Types.String
    }
  },
  techEmployees: [{
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  }],
  initiator: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  skills:[{
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Skill'
  }],
  reviews: [{
    reviewer: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    },
    message: {
      type: String,
      required: true
    },
  }]
});

module.exports = mongoose.model('Proposal', ProposalModelSchema);
