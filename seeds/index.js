const faker = require('Faker');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const moment = require('moment');

const User = require('../models/user.model');
const Role = require('../models/role.model');
const Skill = require('../models/skill.model');
const Proposal = require('../models/proposal.model');

const basicRoles = ['Administrator', 'HR', 'Tech Employee'];
const basicSkills = ['JavaScript', 'TypeScript', 'PHP', 'Angular', 'React', 'Vue', 'ASP.NET'];
const defaultPassword = '123qwe!@#QWE';
const basicStatuses = [
    'Waiting for an interview',
    'Successful interview',
    'Failed interview'
];

const createRoles = async () => {
    return await Role.insertMany(basicRoles.map(role => ({name: role})));
};

const createSkills = async () => {
    return await Skill.insertMany(basicSkills.map(skill => ({name: skill})));
};

const createUsers = async (role, count) => {
    const users = [];
    for(let i =0; i<count; i++){
        users.push({
            firstName: faker.Name.firstName(),
            lastName: faker.Name.lastName(),
            roleId: role._id,
            avatarFilePath: faker.Image.avatar(),
            email: faker.Internet.email(),
            password: await bcrypt.hash(defaultPassword, 8)
        })
    }
    return await User.insertMany(users);
};

const createProposals = async (hrs, techemployees, skills, count) => {
    const proposals = [];
    for(let i = 0; i<count; i++) {
        const relatedTechEmployees = _.sampleSize(techemployees, _.random(1,5));
        const relatedSkills = _.sampleSize(techemployees, _.random(1,5));
        const reviews = [];
        relatedTechEmployees.forEach(user => reviews.push({
            reviewer: user._id,
            message: faker.Lorem.paragraphs(2)
        }));
        proposals.push({
            techEmployees: relatedTechEmployees,
            skills: relatedSkills,
            status: _.sample(basicStatuses),
            initiator: _.sample(hrs)._id,
            desiredSalary: _.random(500, 2000),
            suggestedSalary: _.random(500, 2000),
            scheduledInterviewTime: moment().add(_.random(1,5), 'days').add(_.random(0,24), 'hours'),
            reviews,
            cvFilePath: faker.Image.avatar(),
            candidate: {
                firstName: faker.Name.firstName(),
                lastName: faker.Name.lastName(),
                middleName: faker.Name.lastName(),
                email: faker.Internet.email(),
                phone: `+380${_.random(100000000,999999999)}`,
                avatarFilePath: faker.Image.avatar()
            }
        })
    }
    return await Proposal.insertMany(proposals);
};
module.exports = async () => {
    try {
         const [[administratorRole, HRRole, TechEmployeeRole], skills] = await Promise.all([
             createRoles(),
             createSkills()
         ]);
         const [administrators, hrs, techemployees] = await Promise.all([
            createUsers(administratorRole, 2),
            createUsers(HRRole, 10),
            createUsers(TechEmployeeRole, 20)
         ]);
         await createProposals(hrs, techemployees, skills, 100);
         console.log('OK');
    } catch (e) {
        console.error(e);
    }
};